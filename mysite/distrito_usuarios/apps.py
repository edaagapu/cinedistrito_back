from django.apps import AppConfig


class DistritoUsuariosConfig(AppConfig):
    name = 'distrito_usuarios'
