from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Rol(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=200)
    fecha_inicio = models.DateTimeField()
    fecha_fin = models.DateTimeField(null=True)

class Persona(models.Model):
    cedula = models.IntegerField(primary_key=True)
    telefono = models.IntegerField()

class Empleado(Persona):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    rol = models.ManyToManyField(Rol)
    sueldo = models.IntegerField()

class Cliente(Persona):
    nombres = models.CharField(max_length=100,default="")
    apellidos = models.CharField(max_length=100,default="")
    puntos = models.IntegerField(default=0)