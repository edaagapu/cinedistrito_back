from distrito_usuarios.models import *
from django.contrib.auth.models import User
from distrito_snacks.models import *
from distrito_multiplex.models import *

import json
from django.http import HttpResponse

def get_empleado(cedula):
    respuesta = {}
    cargo = []
    empleado = Empleado.objects.get(cedula=cedula)
    respuesta['identificacion'] = empleado.cedula
    respuesta['nombre'] = empleado.user.first_name
    respuesta['apellido'] = empleado.user.last_name
    respuesta['sueldo'] = empleado.sueldo
    roles = empleado.rol.all()
    for rol in roles:
        car = {}
        car['nombre_rol'] = rol.nombre
        car['descripcion_rol'] = rol.descripcion
        car['fecha_inicio'] = str(rol.fecha_inicio)
        car['fecha_final'] = str(rol.fecha_fin)
        cargo.append(car)
    respuesta['roles'] = cargo
    return respuesta

def generar_multiplex(request):
    try:
        print("Entre")
        body = json.loads(request.body)
        multiplex = Multiplex()
        multiplex.id = body['id']
        multiplex.nombre = body['nombre']
        multiplex.descripcion = body['descripcion']
        multiplex.direccion = body['direccion']
        multiplex.save()

        respuesta = {'ok':True,'message':'Multiplex creado con éxito con id:'+str(multiplex.id)}
    except Exception as e:
        respuesta = {'ok':False,'message': str(e)}
    return HttpResponse(json.dumps(respuesta),content_type='application/json')

def generar_snack(request):
    try:
        body = json.loads(request.body)
        snack = Snack()
        snack.nombre = body['nombre']
        snack.precio = body['precio']
        snack.save()

        multiplex = Multiplex.objects.get(id=body['multiplex'])
        inventario_snack = InventarioSnack()
        inventario_snack.multiplex = multiplex
        inventario_snack.cantidad = body['cantidad']
        inventario_snack.snack = snack
        inventario_snack.save()

        respuesta = {'ok':True,'message':'Snack creado con éxito con id:'+str(snack.id)}
    except Exception as e:
        respuesta = {'ok':False,'message': str(e)}
    return HttpResponse(json.dumps(respuesta),content_type='application/json')

def generar_combo(request):
    try:
        body = json.loads(request.body)
        combo = Combo()
        combo.nombre = body['nombre']

        combo.precio = 0
        combo.descripcion = body['descripcion']
        combo.save()
        productos = body['producto']
        for producto in productos:
            snack = Snack()
            snack.nombre = producto['nombre']
            snack.precio = producto['precio']
            combo.precio += snack.precio
            snack.save()
            combo.producto.add(snack)
        combo.precio = combo.precio*0.9
        combo.save()

        multiplex = Multiplex.objects.get(id=body['multiplex'])
        inventario_combos = InventarioCombos()
        inventario_combos.multiplex = multiplex
        inventario_combos.combo = combo
        inventario_combos.cantidad = body['cantidad']
        inventario_combos.save()

        respuesta = {'ok':True,'message':'Combo creado con éxito'}
    except Exception as e:
        respuesta = {'ok':False,'message': str(e)}
    return HttpResponse(json.dumps(respuesta),content_type='application/json')

def ver_empleado(request,cedula):
    respuesta = get_empleado(cedula)
    return HttpResponse(json.dumps(respuesta),content_type='application/json')


def listar_inventario(request,id):
    respuesta = {}
    multiplex = Multiplex.objects.get(id=id)
    inventario_combos = []
    inventario_snacks = []

    combos = InventarioCombos.objects.filter(multiplex=multiplex)
    snacks = InventarioSnack.objects.filter(multiplex=multiplex)

    for combo in combos:
        res = {}
        res['nombre']=combo.nombre
        res['precio']=combo.precio
        res['descripcion']=combo.descripcion
        r = []
        sns = combo.snack.all()
        for sn in sns:
            e = {}
            e['nombre'] = sn.nombre
            e['precio' ] = sn.precio
            r.append(e)
        res['producto'] = r
        inventario_combos.append(res)
    respuesta['combos'] = inventario_combos
    for snack in snacks:
        res = {}
        res['nombre'] = snack.nombre
        res['precio'] = snack.precio
        res['id'] = snack.id
        inventario_snacks.append(res)
    respuesta['snacks'] = inventario_snacks
    return HttpResponse(json.dumps(respuesta),content_type='application/json')


def listar_empleados(request):
    respuesta = {}
    emp = []
    empleados = Empleado.objects.all()
    for empleado in empleados:
        emp.append(get_empleado(empleado.cedula))
    respuesta["empleados"] = emp
    return HttpResponse(json.dumps(respuesta),content_type='application/json')

def crear_empleado(request):
    respuesta = {}
    try:
        body = json.loads(request.body)
        usuario = User.objects.create_user(body['nombre'][0:2]+body['apellido'],password = body['contrasena'])
        usuario.first_name = body['nombre']
        usuario.last_name = body['apellido']
        usuario.email = body['correo']
        usuario.save()

        rol = Rol()
        rol.nombre = body['nombre_rol']
        rol.descripcion = body['descripcion_rol']
        rol.fecha_inicio = body['fecha_inicio']
        rol.save()

        empleado = Empleado()
        empleado.cedula = body['identificacion']
        empleado.sueldo = body['sueldo']
        empleado.telefono = body['celular']
        empleado.user = usuario

        empleado.save()
        empleado.rol.add(rol)
        empleado.save()
        respuesta = {'ok':True,'message':'Procedimiento terminó bien'}
    except Exception as e:
        respuesta = {'ok':False,'message': str(e)}
    return HttpResponse(json.dumps(respuesta),content_type='application/json')
