from .views import *

from django.urls import path
urlpatterns = [
    path('post/empleados/',crear_empleado,name='crear_empleado'),
    path('post/snacks/', generar_snack, name='generar_snack'),
    path('post/combo/', generar_combo, name='generar_combo'),
    path('post/multiplex/', generar_multiplex, name='generar_multiplex'),

    path('get/empleados/lista/',listar_empleados,name='listar_empleados'),
    path('get/empleados/<int:cedula>',ver_empleado,name='ver_empleado'),
    path('get/inventario/<int:id>',listar_inventario,name='inventario_productos'),
]