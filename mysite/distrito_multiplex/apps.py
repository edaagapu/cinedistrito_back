from django.apps import AppConfig


class DistritoMultiplexConfig(AppConfig):
    name = 'distrito_multiplex'
