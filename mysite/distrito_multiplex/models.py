from django.db import models
from distrito_snacks.models import Snack,Combo

# Create your models here.
class Multiplex(models.Model):
    id = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=100)
    direccion = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=200)

class Sala(models.Model):
    id = models.CharField(primary_key=True,max_length=50)
    cine = models.OneToOneField(Multiplex,on_delete=models.CASCADE)

class Silla(models.Model):
    id = models.CharField(primary_key=True, max_length=50)
    sala = models.OneToOneField(Sala, on_delete=models.CASCADE)

class InventarioSnack(models.Model):
    multiplex = models.ForeignKey(Multiplex,on_delete=models.CASCADE)
    snack = models.ForeignKey(Snack,on_delete=models.CASCADE)
    cantidad = models.IntegerField()

class InventarioCombos(models.Model):
    multiplex = models.ForeignKey(Multiplex,on_delete=models.CASCADE)
    combo = models.ForeignKey(Combo,on_delete=models.CASCADE)
    cantidad = models.IntegerField()