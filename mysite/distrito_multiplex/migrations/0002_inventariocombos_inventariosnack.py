# Generated by Django 3.0.8 on 2020-08-19 23:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('distrito_snacks', '0002_combo_producto'),
        ('distrito_multiplex', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='InventarioSnack',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cantidad', models.IntegerField()),
                ('multiplex', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='distrito_multiplex.Multiplex')),
                ('snack', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='distrito_snacks.Snack')),
            ],
        ),
        migrations.CreateModel(
            name='InventarioCombos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cantidad', models.IntegerField()),
                ('multiplex', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='distrito_multiplex.Multiplex')),
                ('snack', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='distrito_snacks.Combo')),
            ],
        ),
    ]
