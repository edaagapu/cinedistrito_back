from django.db import models

# Create your models here.
class Snack(models.Model):
    nombre = models.CharField(max_length=100)
    precio = models.IntegerField()

class Combo(models.Model):
    nombre = models.CharField(max_length=100)
    precio = models.IntegerField()
    descripcion = models.CharField(max_length=200)
    producto = models.ManyToManyField(Snack)
