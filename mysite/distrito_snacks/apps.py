from django.apps import AppConfig


class DistritoSnacksConfig(AppConfig):
    name = 'distrito_snacks'
