from django.db import models
from distrito_multiplex.models import Multiplex,Sala,Silla
from distrito_usuarios.models import Cliente
# Create your models here.
class Genero(models.Model):
    nombre = models.CharField(max_length=100)

class Pelicula(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=200)
    genero = models.ManyToManyField(Genero)


class Evaluacion(models.Model):
    pelicula = models.ForeignKey(Pelicula,on_delete=models.CASCADE)
    multiplex = models.OneToOneField(Multiplex,on_delete=models.CASCADE)
    comentario = models.CharField(max_length=200)
    usuario = models.ForeignKey(Cliente,on_delete=models.CASCADE)

class Proyeccion(models.Model):
    hora = models.DateTimeField()
    pelicula = models.ForeignKey(Pelicula,on_delete=models.CASCADE)
    sala = models.ForeignKey(Sala,on_delete=models.CASCADE)

class Tiquete(models.Model):
    proyeccion = models.OneToOneField(Proyeccion,on_delete=models.CASCADE)
    silla = models.OneToOneField(Silla,on_delete=models.CASCADE)
    cliente = models.OneToOneField(Cliente,on_delete=models.CASCADE)
    puntos = models.IntegerField(default=15)