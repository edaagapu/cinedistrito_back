from django.apps import AppConfig


class DistritoMoviesConfig(AppConfig):
    name = 'distrito_movies'
