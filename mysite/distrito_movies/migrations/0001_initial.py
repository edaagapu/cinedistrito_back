# Generated by Django 3.0.8 on 2020-08-03 16:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('distrito_multiplex', '0001_initial'),
        ('distrito_usuarios', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Genero',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Pelicula',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('descripcion', models.CharField(max_length=200)),
                ('genero', models.ManyToManyField(to='distrito_movies.Genero')),
            ],
        ),
        migrations.CreateModel(
            name='Proyeccion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hora', models.DateTimeField()),
                ('pelicula', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='distrito_movies.Pelicula')),
                ('sala', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='distrito_multiplex.Sala')),
            ],
        ),
        migrations.CreateModel(
            name='Tiquete',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('puntos', models.IntegerField(default=15)),
                ('cliente', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='distrito_usuarios.Cliente')),
                ('proyeccion', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='distrito_movies.Proyeccion')),
                ('silla', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='distrito_multiplex.Silla')),
            ],
        ),
        migrations.CreateModel(
            name='Evaluacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comentario', models.CharField(max_length=200)),
                ('multiplex', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='distrito_multiplex.Multiplex')),
                ('pelicula', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='distrito_movies.Pelicula')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='distrito_usuarios.Cliente')),
            ],
        ),
    ]
